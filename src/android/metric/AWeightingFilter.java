package android.metric;

import java.util.Hashtable;

public class AWeightingFilter {

    private static final Hashtable<Integer, AWeightingFilter> aWeightingFilters = new Hashtable<Integer, AWeightingFilter>();

    static {
        /*
         * A-Weighting coefficients were computed with MatLab using the "adsgn" function from
         * the "octave" toolbox by Christophe Couvreur (Faculte Polytechnique de Mons):
         * 		http://www.mathworks.com/matlabcentral/fileexchange/69
         * Alternatively they can also be computed with Python (with numpy and scipy libraries)
         * using this script (which is a port of the MatLab function mentioned above):
         * 		https://gist.github.com/148112
         */
        // 8000 Hz
        aWeightingFilters.put(8000,
                new AWeightingFilter(8000,
                        new double[]{1.0d, -2.1284671930091217D, 0.29486689801012067D, 1.8241838307350515D, -0.80566289431197835D, -0.39474979828429368D, 0.20985485460803321D},
                        new double[]{0.63062094682387282D, -1.2612418936477434D, -0.63062094682387637D, 2.5224837872954899D, -0.6306209468238686D, -1.2612418936477467D, 0.63062094682387237D}));
        // 16000 Hz
        aWeightingFilters.put(16000,
                new AWeightingFilter(16000,
                        new double[]{1.0d, -2.867832572992166100D, 2.221144410202319500D, 0.455268334788656860D, -0.983386863616282910D, 0.055929941424134225D, 0.118878103828561270D},
                        new double[]{0.53148982982355708D, -1.0629796596471122D, -0.53148982982356319D, 2.1259593192942332D, -0.53148982982355686D, -1.0629796596471166D, 0.53148982982355797D}));
        // 22050 Hz
        aWeightingFilters.put(22050,
                new AWeightingFilter(22050,
                        new double[]{1.0d, -3.2290788052250736D, 3.3544948812360302D, -0.73178436806573255D, -0.6271627581807262D, 0.17721420050208803D, 0.056317166973834924D},
                        new double[]{0.44929985042991927D, -0.89859970085984164D, -0.4492998504299115D, 1.7971994017196726D, -0.44929985042992043D, -0.89859970085983754D, 0.44929985042991943D}));
        // 24000 Hz
        aWeightingFilters.put(24000,
                new AWeightingFilter(24000,
                        new double[]{1.0d, -3.3259960042, 3.6771610793, -1.1064760768, -0.4726706735, 0.1861941760, 0.0417877134},
                        new double[]{0.4256263893, -0.8512527786, -0.4256263893, 1.7025055572, -0.4256263893, -0.8512527786, 0.4256263893}));
        // 32000 Hz
        aWeightingFilters.put(32000,
                new AWeightingFilter(32000,
                        new double[]{1.0d, -3.6564460432, 4.8314684507, -2.5575974966, 0.2533680394, 0.1224430322, 0.0067640722},
                        new double[]{0.3434583387, -0.6869166774, -0.3434583387, 1.3738333547, -0.3434583387, -0.6869166774, 0.3434583387}));
        // 44100 Hz
        aWeightingFilters.put(44100,
                new AWeightingFilter(44100,
                        new double[]{1.0d, -4.0195761811158315D, 6.1894064429206921D, -4.4531989035441155D, 1.4208429496218764D, -0.14182547383030436D, 0.0043511772334950787D},
                        new double[]{0.2557411252042574D, -0.51148225040851436D, -0.25574112520425807D, 1.0229645008170318D, -0.25574112520425918D, -0.51148225040851414D, 0.25574112520425729D}));
        // 48000 Hz
        aWeightingFilters.put(48000,
                new AWeightingFilter(
                        48000,
                        new double[]{1.0d, -4.113043408775872D, 6.5531217526550503D, -4.9908492941633842D, 1.7857373029375754D, -0.24619059531948789D, 0.011224250033231334D},
                        new double[]{0.2343017922995132D, -0.4686035845990264D, -0.23430179229951431D, 0.9372071691980528D, -0.23430179229951364D, -0.46860358459902524D, 0.23430179229951273D}));
    }

    private int sampleRate;

    private int order;
    private double[] coefficientA;
    private double[] coefficientB;
    private double[] conditions;

    private boolean keepConditions;

    private AWeightingFilter(int sampleRate, double[] CoefficientA, double[] coefficientB) {
        this(sampleRate, CoefficientA, coefficientB, false); //not keeping conditions is default
    }

    private AWeightingFilter(int sampleRate, double[] aCoefficient, double[] coefficientB, boolean keepConditions) {
        if (aCoefficient.length != coefficientB.length) {
            throw new IllegalArgumentException("Mismatch in number of A and B coefficients");
        }
        if (aCoefficient[0] != 1.0d) {
            throw new IllegalArgumentException("Coefficients must be normalized (a_0 must be = 1");
        }
        this.sampleRate = sampleRate;
        this.order = aCoefficient.length - 1; // coefficientB.length - 1
        this.coefficientA = aCoefficient;
        this.coefficientB = coefficientB;
        this.keepConditions = keepConditions;
        this.conditions = new double[order]; //initialize conditions (all 0.0d)
    }

    /**
     * Apply A weighting filtering on input
     *
     * @param input array of sound samples (in double)
     * @return filtered array of sound samples (in double)
     */
    public double[] apply(double[] input) {
        double[] output = new double[input.length];
        for (int i = 0; i < input.length; i++) {
            double x_i = input[i];
            //Filter sample:
            double y_i = x_i * coefficientB[0] + conditions[0];
            //Store filtered sample:
            output[i] = y_i;
            //Adjust conditions:
            // all but the last condition:
            for (int j = 0; j < order - 1; j++) {
                conditions[j] = x_i * coefficientB[j + 1] - y_i * coefficientA[j + 1] + conditions[j + 1];
            }
            // last condition:
            conditions[order - 1] = x_i * coefficientB[order] - y_i * coefficientA[order];
        }
        if (!keepConditions) {
            conditions = new double[order]; // reset conditions
        }
        return output;
    }

    /**
     * Get A Weighting filter based on sample rate
     *
     * @param sampleRate sample rate (in Hz)
     * @return A Weighting filter instance
     */
    static public AWeightingFilter getAWeightingFilter(int sampleRate) {
        return aWeightingFilters.get(sampleRate);
    }
}
