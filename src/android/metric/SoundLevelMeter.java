package android.metric;

public class SoundLevelMeter {

    private DataTypeConverter converter;
    private AWeightingFilter filter;

    public SoundLevelMeter(final int sampleRate){
        this.converter = new DataTypeConverter();
        filter = AWeightingFilter.getAWeightingFilter(sampleRate);
    }


    /**
     * Analyse of the array of sound samples and calculate DB (A) value
     *
     * @param samples array of sound samples (short)
     * @return DB (A) value
     * @throws Exception in case of error
     */
    public double analyseSamples(final short[] samples) throws Exception {
        final double[] convertedSamples = converter.convertShortsToDoubles(samples); // convert input from short to double
        final double[] filteredSamples = filter.apply(convertedSamples); // apply A weighting filter
        return computeLeq(filteredSamples); // compute Leq
    }


    /**
     * Compute Leq from the array of samples
     * <p/>
     * Leq = 10*log10[(1/T)*S[0,T](pt/p0)^2)] dt
     * with S[x,y]=integral over interval [x,y]
     * and p0 = 2x10^-5 = 0,00002
     * <p/>
     * Integral computed as a Riemann sum (http://en.wikipedia.org/wiki/Riemann_integral):
     * <p/>
     * Leq = 10*log10[(1/T)*E[0,T]((pt/p0)^2)]
     * = 10*log10[(1/T)*E[0,T]((pt*(1/p0))^2)]
     * = 10*log10[(1/T)*E[0,T](pt^2*(1/p0)^2)]
     * = 10*log10[(1/T)*E[0,T](pt^2)*(1/p0)^2]
     * = 10*log10[(1/T)*E[0,T](pt^2)*(1/p0)^2]
     * = 10*[log10[(1/T)*E[0,T](pt^2)] + log10[(1/p0)^2]]
     * = 10*log10[(E[0,T](pt^2)/T] + 10*log10[(1/p0)^2]
     * = 10*log10[(E[0,T](pt^2)/T] + 20*log10[(1/p0)]
     * = 10*log10[(E[0,T](pt^2)/T] + 20*log10[50000]
     * = 10*log10[(E[0,T](pt^2)/T] + 93.97940008672037609572522210551
     * Log info: http://oakroadsystems.com/math/loglaws.htm
     *
     * @param samples array of sound samples (double)
     * @return the Leq
     */
    private double computeLeq(double[] samples) throws Exception {
        double sumSquare = 0.0F;
        for (int i = 0; i < samples.length; i++) {
            sumSquare += samples[i] * samples[i];
        }
        double leq = (10.0D * MathNT.log10(sumSquare / samples.length)) + 93.97940008672037609572522210551D;
        if (Double.isNaN(leq) || leq <= 0) {
            throw new IllegalResultException("Leq is NaN, negative or zero: " + leq);
        }
        return leq;
    }

    private class IllegalResultException extends Exception {

        private static final long serialVersionUID = -3972402768127493264L;

        public IllegalResultException(String message) {
            super(message);
        }

    }
}
