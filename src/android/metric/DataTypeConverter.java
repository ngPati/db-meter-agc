package android.metric;

public class DataTypeConverter {

    public DataTypeConverter() {
    }

    /**
     * Converting array of input in short datatype to array of output in double datatype
     *
     * @param input array of shorts
     * @return array of doubles
     */
    public double[] convertShortsToDoubles(final short[] input) {
        double[] output = new double[input.length];
        for (int i = 0; i < input.length; i++) {
            output[i] = decodeShortToDouble(input[i]);
        }
        return output;
    }

    /**
     * Decode short input to double output.
     * Short is converted to long data type and divided by {@link java.lang.Short#MIN_VALUE} or {@link java.lang.Short#MAX_VALUE}
     * depends on negative or positive value of input.
     *
     * @param input input value in short datatype
     * @return output value in double datatype
     */
    private double decodeShortToDouble(final short input) {
        long longOutput = (long) input;

        return (double) longOutput / (longOutput < 0 ? (Short.MIN_VALUE * -1.0D) : Short.MAX_VALUE);
    }
}
